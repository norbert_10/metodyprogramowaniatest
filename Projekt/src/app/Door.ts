import { Zone } from "./Zones/Zone";
import { Card } from "./Card";

export class Door
{
    constructor(zoneOut: Zone, zoneIn: Zone)
    {
       this.ZoneOut = zoneOut
       this.ZoneIn = zoneIn;
    }
    
    ZoneOut: Zone;
    ZoneIn: Zone;

    public Open(card: Card) : void
    {
        if(this.ZoneIn.ComeIn(card) === true)
        {
            this.ZoneOut.ComeOut(card);
        };
    }
}