export enum CardType 
{
    Manager = "Manager",
    AirstripWorker = "AirstripWorker",
    SortWorker = "SortWorker",
    TransportWorker = "TransportWorker",
    Janitor = "Janitor"
}