import { Zone } from "./Zones/Zone";
import { Card } from "./Card";
import { Door } from "./Door";
import { CardType } from "./CardType";
import { IEntryConditions } from "./Zones/IEntryConditions";
import { EntryConditionsWithLimitedWorkers } from "./Zones/EntryConditionsWithLimitedWorkers";
import { EntryConditionsWithoutLimits } from "./Zones/EntryConditionsWithoutLimits";
import { EntryConditionsWithLimitedOtherWorkers } from "./Zones/EntryConditionsWithLimitedOtherWorkers";

export class Airport
{
    constructor(name: string)
    {
        this.Name = name;
        
        this.Init();
    }
    
    public Name: string;
    public Cards: Array<Card>=[];
    public Zones: Array<Zone>=[];
    public Door: Array<Door>=[]
    public ZoneDefault: Zone;

    public IssueCard(numberCard: number, name: string, surname: string, cardType: CardType) : Card
    {
        var card = new Card(numberCard, name, surname, cardType);
        this.Cards = this.Cards.concat(card);
        this.ZoneDefault.ComeIn(card);

        return card;
    }

    private Init() : void
    {
        var airstripZone = this.CreateAirStripZone();
        var sortZone = this.CreateSortZone();
        var warehouseZone = this.CreateWarehouseZone();
        var loadingZone = this.CreateLoadingZone();
        var outsideZone = this.CreateOutsideZone();

        this.ZoneDefault = outsideZone;
        this.Zones.push(airstripZone);
        this.Zones.push(sortZone);
        this.Zones.push(warehouseZone);
        this.Zones.push(loadingZone);
        this.Zones.push(outsideZone);

        var door0 = new Door(outsideZone, loadingZone);
        var door1 = new Door(loadingZone, outsideZone);
        var door2 = new Door(sortZone, loadingZone);
        var door3 = new Door(loadingZone, sortZone);
        var door4 = new Door(airstripZone, sortZone);
        var door5 = new Door(sortZone, airstripZone);
        var door6 = new Door(sortZone, warehouseZone);
        var door7 = new Door(warehouseZone, sortZone);

        this.Door.push(door0);
        this.Door.push(door1);
        this.Door.push(door2);
        this.Door.push(door3);
        this.Door.push(door4);
        this.Door.push(door5);
        this.Door.push(door6);
        this.Door.push(door7);
    }

    CreateAirStripZone() : Zone
    {
        var maxCapicity = 3;
        var entryConditionsManager = new EntryConditionsWithLimitedWorkers(CardType.Manager, maxCapicity);
        var entryConditionsAirstripWorker =
        new EntryConditionsWithLimitedWorkers(CardType.AirstripWorker, maxCapicity);
        
        var conditions = new Array<IEntryConditions>()

        conditions.push(entryConditionsManager);
        conditions.push(entryConditionsAirstripWorker);
        

        var zone = new Zone("Airstrip", conditions);
        
        return zone;
    }

    CreateSortZone() : Zone
    {
        var maxCapicity = 7;
        var entryConditionsManager = new EntryConditionsWithoutLimits(CardType.Manager);
        var entryConditionsAirstripWorker = new EntryConditionsWithLimitedWorkers(CardType.AirstripWorker, maxCapicity);
        var entryConditionsSortWorker = new EntryConditionsWithLimitedWorkers(CardType.SortWorker, maxCapicity);
        var entryConditionsJanitor = new EntryConditionsWithLimitedOtherWorkers(CardType.Janitor, maxCapicity, 1);

        var conditions = new Array<IEntryConditions>()

        conditions.push(entryConditionsManager);
        conditions.push(entryConditionsAirstripWorker);
        conditions.push( entryConditionsSortWorker);
        conditions.push(entryConditionsJanitor);

        var zone = new Zone("Sort", conditions);
        return zone;
    }

    CreateWarehouseZone() : Zone
    {
        var maxCapicity = 3;
        var entryConditionsManager = new EntryConditionsWithoutLimits(CardType.Manager);
        var entryConditionsSortWorker = new EntryConditionsWithLimitedWorkers(CardType.SortWorker, maxCapicity);
        var entryConditionsJanitor = new EntryConditionsWithLimitedOtherWorkers(CardType.Janitor, maxCapicity, 1);

        var conditions = new Array<IEntryConditions>()

        conditions.push(entryConditionsManager);
        conditions.push( entryConditionsSortWorker);
        conditions.push(entryConditionsJanitor);

        var zone = new Zone("Warehouse", conditions);
        return zone;
    }

    CreateLoadingZone() : Zone
    {
        var maxCapicity = 5;
        var entryConditionsManager = new EntryConditionsWithoutLimits(CardType.Manager);
        var entryConditionsAirstripWorker = new EntryConditionsWithLimitedWorkers(CardType.AirstripWorker, maxCapicity);
        var entryConditionsSortWorker = new EntryConditionsWithLimitedWorkers(CardType.SortWorker, maxCapicity);
        var conditionsWejsciaTransportWorker = new EntryConditionsWithLimitedWorkers(CardType.TransportWorker, maxCapicity);
        var entryConditionsJanitor = new EntryConditionsWithLimitedOtherWorkers(CardType.Janitor, maxCapicity, 1);

        var conditions = new Array<IEntryConditions>()

        conditions.push(entryConditionsManager);
        conditions.push(entryConditionsAirstripWorker);
        conditions.push( entryConditionsSortWorker);
        conditions.push(conditionsWejsciaTransportWorker);
        conditions.push(entryConditionsJanitor);


        var zone = new Zone("Loading/Unloading", conditions);
        return zone;
    }

    CreateOutsideZone() : Zone
    {
        var entryConditionsManager = new EntryConditionsWithoutLimits(CardType.Manager);
        var entryConditionsAirstripWorker = new EntryConditionsWithoutLimits(CardType.AirstripWorker);
        var entryConditionsSortWorker = new EntryConditionsWithoutLimits(CardType.SortWorker);
        var conditionsWejsciaTransportWorker = new EntryConditionsWithoutLimits(CardType.TransportWorker);
        var entryConditionsJanitor = new EntryConditionsWithoutLimits(CardType.Janitor);

        var conditions = new Array<IEntryConditions>()

        conditions.push(entryConditionsManager);
        conditions.push(entryConditionsAirstripWorker);
        conditions.push( entryConditionsSortWorker);
        conditions.push(conditionsWejsciaTransportWorker);
        conditions.push(entryConditionsJanitor);

        var zone = new Zone("Outside", conditions);
        return zone;
    }
}