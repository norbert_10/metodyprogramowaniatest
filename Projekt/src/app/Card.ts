import { CardType } from "./CardType";

export class Card
{
    constructor(number: number, name: string, surname: string, cardType: CardType)
    {
        this.Number = this.NumberCardValidator(cardType, number);
        this.Name = name;
        this.Surname = surname;
        this.CardType = cardType;
    }

    public Number: number;
    public Name: string;
    public Surname: string;
    public CardType: CardType;

    public NumberCardValidator(cardType: CardType, number: number) : number //numberCard Validator
    {
         if(CardType.Manager === cardType && (number > 0 && number <=99))
            return number;
         else if(CardType.AirstripWorker === cardType && (number >= 100 && number <= 200))
            return number;
         else if(CardType.SortWorker === cardType && (number >= 201 && number <= 500))
            return number;
         else if(CardType.TransportWorker === cardType && (number >= 501 && number <= 999))
            return number;
         else if(CardType.Janitor === cardType && (number >= 1000))
            return number;
         else
         {
            console.error('Nie spełniono warunkow do utworzenia karty');
            return null;
         }
    }
}