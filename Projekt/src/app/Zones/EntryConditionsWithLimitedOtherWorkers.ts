import { CardType } from "../CardType";
import { Card } from "../Card";
import { IEntryConditions } from "./IEntryConditions";

export class EntryConditionsWithLimitedOtherWorkers implements IEntryConditions
{
   constructor
   (
       cardType: CardType, 
       maxNumberLimit : number, 
       minNumberOtherWorkers  : number
    )
   {
        this.cardType = cardType;
        this.MaxNumberLimit = maxNumberLimit;
        this.MinNumberOtherWorkers  = minNumberOtherWorkers; 
   }

   cardType: CardType;
   public MaxNumberLimit : number;
   public MinNumberOtherWorkers : number;

   public CheckConditions(currentCardsInZone: Array<Card>) : boolean
   {
       if(currentCardsInZone.length >= this.MaxNumberLimit)
            return false;
        
        var numberOtherWorkers = currentCardsInZone.filter(q=>q.CardType != this.cardType).length;
        if(numberOtherWorkers < this.MinNumberOtherWorkers)
            return false;

        return true;
   }
}