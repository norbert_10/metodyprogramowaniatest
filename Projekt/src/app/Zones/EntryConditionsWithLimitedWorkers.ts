import { CardType } from "../CardType";
import { Card } from "../Card";
import { IEntryConditions } from "./IEntryConditions";

export class EntryConditionsWithLimitedWorkers implements IEntryConditions
{
   constructor(cardType: CardType, maxNumberLimit : number)
   {
        this.cardType = cardType;
        this.MaxNumberLimit = maxNumberLimit;
   }

   cardType: CardType;
   public MaxNumberLimit : number;

   public CheckConditions(currentCardsInZone: Array<Card>) : boolean
   {
       if(currentCardsInZone.length >= this.MaxNumberLimit)
          return false;

       return true;
   }
}