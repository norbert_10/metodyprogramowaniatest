import { Card } from "../Card";
import { IEntryConditions } from "./IEntryConditions";

export class Zone
{
    constructor(name: string, entryConditions: IEntryConditions[])
    {
        this.Name = name;
        this._entryConditions = entryConditions;
    }

    public Name: string;
    private _cards: Array<Card>=[];
    private _entryConditions: Array<IEntryConditions> = [];

    public ComeIn(card: Card) : boolean
    {
        var entryConditionsTypeCard = this._entryConditions.filter(q => q.cardType === card.CardType)[0];

        if(entryConditionsTypeCard  != null)
        {
            if(entryConditionsTypeCard.CheckConditions(this._cards))
            {
                this._cards.push(card);

                return true;
            }
        }
        console.error("Nie spełniono warunków wejścia");
        return false;
    }

    public ComeOut(card: Card) : boolean
    {
        var removeIndex = this._cards.map(item => item.Number).indexOf(card.Number);
        if(removeIndex >=-1)
        {
            this._cards.splice(removeIndex, 1);
            return true;
        }
        else
        console.error('nie mozna usunac');

        return false;
    }

    public ShowCardsInSide()
    {
        return this._cards;
    }
}