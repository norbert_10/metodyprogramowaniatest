import { CardType } from "../CardType";
import { Card } from "../Card";
import { IEntryConditions } from "./IEntryConditions";

export class EntryConditionsWithoutLimits implements IEntryConditions
{
   constructor(cardType: CardType)
   {
        this.cardType = cardType;
   }
   cardType: CardType;

   public CheckConditions(currentCardsInZone: Array<Card>) : boolean
   {
       return true;
   }
}