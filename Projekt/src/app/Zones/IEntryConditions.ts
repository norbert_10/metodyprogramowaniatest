import { CardType } from "../CardType";
import { Card } from "../Card";

export interface IEntryConditions
{
    cardType: CardType;

    CheckConditions(currentCardsInZone: Array<Card>) : boolean;
}