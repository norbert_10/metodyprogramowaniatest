import { Airport } from "./Airport";
import { CardType } from "./CardType";

export class Tester
{
  airPort : Airport = new Airport("Centrum Warsaw Airpost Logistics");

  constructor()
  {
    var card1 = this.airPort.IssueCard(22, "Remigiusz", "Mazurek", CardType.Manager);
    var card2 = this.airPort.IssueCard(1128, "Kornel", "Chmielewski", CardType.Janitor);
    var card3 = this.airPort.IssueCard(1032, "Marcel", "Malinowski", CardType.Janitor);
    var card4 = this.airPort.IssueCard(230, "Dobromił", "Baran", CardType.SortWorker);
    var card5 = this.airPort.IssueCard(412, "Alex", "Ostrowski", CardType.SortWorker);
    var card6 = this.airPort.IssueCard(351, "Amir", "Woźniak", CardType.SortWorker);
    var card7 = this.airPort.IssueCard(254, "Gniewomir", "Wróblewski", CardType.SortWorker);
    var card8 = this.airPort.IssueCard(123, "Dorian", "Kowalski", CardType.AirstripWorker);
    var card9 = this.airPort.IssueCard(107, "Maurycy", "Sokołowski", CardType.AirstripWorker);
    var card10 = this.airPort.IssueCard(186, "Ariel", "Szczepański", CardType.AirstripWorker);
    var card11 = this.airPort.IssueCard(665, "Cezary", "Kaźmierczak", CardType.TransportWorker);
    var card12 = this.airPort.IssueCard(725, "Błażej", "Szulc", CardType.TransportWorker);
    var card13 = this.airPort.IssueCard(33, "Zuzanna", "Grobelna", CardType.Manager);

    //Test 1 - Przenieś kartę pracownika ze strefy zewnętrznej do strefy rozładunku/załadunku
    console.log('Test 1 - Przenieś kartę pracownika ze strefy zewnętrznej do strefy rozładunku/załadunku.');

    this.airPort.Door[0].Open(card1);

    this.GetWorkersInZones();

    //Test 2 - Przenieś kartę ze strefy sortowania do strefy magazynowania.
    console.log('Test 2 - Przenieś kartę ze strefy sortowania do strefy magazynowania.');

    this.airPort.Door[0].Open(card4);
    this.airPort.Door[3].Open(card4);
    this.airPort.Door[6].Open(card4);

    this.GetWorkersInZones();

    //Test 3 - W strefie Airstrip nie przebywa więcej niż 3 pracowników.
    console.log('Test 3 - W strefie Airstrip nie przebywa więcej niż 3 pracowników');

    this.airPort.Door[0].Open(card8);
    this.airPort.Door[0].Open(card13);
    this.airPort.Door[0].Open(card9);
    this.airPort.Door[0].Open(card10);

    this.airPort.Door[3].Open(card8);
    this.airPort.Door[3].Open(card13);
    this.airPort.Door[3].Open(card9);
    this.airPort.Door[3].Open(card10);

    this.airPort.Door[5].Open(card8);
    this.airPort.Door[5].Open(card13);
    this.airPort.Door[5].Open(card9);
    this.airPort.Door[5].Open(card10);
   
    this.GetWorkersInZones();

    //Test 4 - Drzwi nie pozwolą na wejście Dozorcy, jeśli w Strefie nie ma innego pracownika..
    console.log('Test 4 - Drzwi nie pozwolą na wejście Dozorcy, jeśli w Strefie nie ma innego pracownika.');

    this.airPort.Door[2].Open(card10);
    this.airPort.Door[3].Open(card3);
 
    this.GetWorkersInZones();

    //Test 5 - W Strefie Sortowni nie może przebywać pracownik Transportu.
    console.log('Test 5 - W Strefie Sortowni nie może przebywać pracownik Transportu..');

    this.airPort.Door[3].Open(card12);

    this.GetWorkersInZones();
  }

  public GetWorkersInZones()
  {
    console.log('Cards in Outside zone',this.airPort.Zones[4].ShowCardsInSide());
    console.log('Cards in Loading zone',this.airPort.Zones[3].ShowCardsInSide());
    console.log('Cards in Warehouse zone',this.airPort.Zones[2].ShowCardsInSide());
    console.log('Cards in Sort zone',this.airPort.Zones[1].ShowCardsInSide());
    console.log('Cards in Airstrip zone',this.airPort.Zones[0].ShowCardsInSide());
  }
}