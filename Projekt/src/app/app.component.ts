import { Component } from '@angular/core';
import {Tester} from './Tester';
import {Card} from './Card';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent{
  title = 'Centrum Warsaw Airpost Logistics'; 
  object = new Tester();

  kartyZewnetrzna = this.object.airPort.Zones[4].ShowCardsInSide();
  kartyAirstrip = this.object.airPort.Zones[0].ShowCardsInSide();
  kartySortownia = this.object.airPort.Zones[1].ShowCardsInSide();
  kartyMagazyn = this.object.airPort.Zones[2].ShowCardsInSide();
  kartyZaladunekRozladunek = this.object.airPort.Zones[3].ShowCardsInSide();
  
  constructor() {
  }

  Przejdz(karta: Card, numerDrzwi: number) : void
  {
    return this.object.airPort.Door[numerDrzwi].Open(karta);
  }
}
